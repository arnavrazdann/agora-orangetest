# Contributing to 

**Thank you for your interest in . Your contributions are highly welcome.**

## Ground Rules

### We Develop with GitLab

We use [GitLab DIOD](https://gitlab.tech.orange) to host code, to track [issues](../../issues) as well as
accept [merge requests](../../merge_requests).

### We Use Merge Requests

Merge requests are the best way to propose changes to the codebase. We actively welcome yours.

### Use conventional git commits

We are using [semantic-release](https://semantic-release.gitbook.io/) to manage our releases with the help of the [Gitlab CI template](https://to-be-continuous.gitlab.io/doc/ref/semantic-release/)

This nice tool automatically takes care or version management, package publishing and release note generation, based on [Commit message syntax](https://semantic-release.gitbook.io/semantic-release/#commit-message-format).

## How to contribute

Check the list of open [issues](../../issues). Either assign an existing issue to yourself, or create a new one that you would like work on and discuss your ideas and use cases.

It is always best to discuss your plans beforehand, to ensure that your contribution is in line with our goals.

1. [Fork the repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)
   and **[create your branch](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-new-branch)
   from `develop` if it exists** otherwise use `master`
2. [Open a new merge request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream) from your fork to the parent repository
3. If you've added code that should be tested, add tests
4. Add or update documentation when it's needed
5. Ensure the test suite passes and make sure your code lints
6. Project maintainers might comment on your work as you progress
7. When you are done, ping a [maintainers](MAINTAINERS) for a review of your merge request

**Thanks for your contributions!**

## How to report a bug

Reporting bugs is one of the best ways to contribute. Before creating a bug report, please check that
an [issue](/issues) reporting the same problem does not already exist. If there is such an issue, you may add your
information as a comment.

To report a new bug you should [open an issue](../issues/new?issuable_template=bug) that summarizes the bug, it's
that easy!

If you want to provide a fix along with your bug report: That is great! In this case please send us a merge request as
described in section [How to contribute](#how-to-contribute).

## How to suggest a feature or enhancement

To request a new feature you should [open an issue](../issues/new?issuable_template=feature) and summarize the
desired functionality and its use case.

**Have fun, and happy coding!**

