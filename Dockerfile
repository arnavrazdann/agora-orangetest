## base image
### multi-stage build (https://sysdig.com/blog/dockerfile-best-practices/#2-1)
FROM python:3.9-alpine AS builder

## install dependencies
# hadolint ignore=DL3018
RUN apk add --no-cache gcc musl-dev libffi-dev openssl-dev

## virtualenv
RUN python3 -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

## add and install requirements
### use COPY instead of ADD (https://sysdig.com/blog/dockerfile-best-practices/#3-2)
### be cautious with the build context (https://sysdig.com/blog/dockerfile-best-practices/#3-3)
COPY ./requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt

## final image
FROM python:3.9-alpine

### use labels (https://sysdig.com/blog/dockerfile-best-practices/#4-2)
LABEL name="Example Web Server" \
      maintainer="bertrand.goareguer@orange.com"

#
## copy Python dependencies from build image
COPY --from=builder /opt/venv /opt/venv

## set working directory
WORKDIR /usr/src/app

# add user
RUN addgroup -g 1000 -S user && \
    adduser -h /usr/src/app -u 1000 -S user -G user

### switch to non-root user (https://sysdig.com/blog/dockerfile-best-practices/#1-1)
USER user

## set environment variables
ENV PATH="/opt/venv/bin:$PATH"

EXPOSE 5000

## run server
ENTRYPOINT [ "python", "/usr/src/app/main.py" ]

### healthcheck (https://sysdig.com/blog/dockerfile-best-practices/#5-5)
HEALTHCHECK \
  CMD wget -q http://127.0.0.1:5000/ -O - | grep "Hello World!" || exit 1

## add app
### use COPY instead of ADD (https://sysdig.com/blog/dockerfile-best-practices/#3-2)
### be cautious with the build context (https://sysdig.com/blog/dockerfile-best-practices/#3-3)
### layer ordering (https://sysdig.com/blog/dockerfile-best-practices/#4-1)
COPY ./main.py requirements.txt /usr/src/app/


